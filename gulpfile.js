"use strict";

const gulp = require('gulp');
const path = require('path');
const reqDir = require('requiredir');

reqDir(path.join(__dirname, './gulp'), { recurse: true });

gulp.task('default', ['build'], () => {
  // nothing
});
