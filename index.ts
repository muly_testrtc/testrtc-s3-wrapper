"use strict";

const AWS = require('aws-sdk');
import * as stream from 'stream';
import * as intoStream from 'into-stream';
import * as fs from 'fs';
// AWS.config.apiVersions = { s3: '2006-03-01' };

// TODO: params for aws type
interface IStorage {
    save(name: any, readStream: any): Promise<any>;
    read(name: any, writeStream: any): Promise<any>;
    readAsObject(params: any): Promise<Object>;
    readAsBuffer(params): Promise<Buffer>;
    list(params): Promise<any>;
    delete(name: string): Promise<any>; // Bucket, Key
}

interface IS3Creds {
  accessKeyId: string;
  secretAccessKey: string;
  region: string
}

interface IOptions {
  accessKeyId: string;
  secretAccessKey: string;
  region: string;
  maxRetries?: number;
}

interface IFile {
    fullBlobName: string;
    properties: any;
    metadata: any;
}

export class S3Storage implements IStorage {
  private _s3: any;
  private BUCKET = 'rtctmp';
  private _params = {};
  private _awsParams;

  constructor(creds: IS3Creds, params = {}) {

    const _defaultConfig = {
      signatureVersion: 'v4',
      maxRetries: 3
    };

    // INFO: Bucket, Key
    this._params = params;

    if (!creds) {
      throw new Error(`No credentials for S3 instance`);
    }

    const _s3Params = Object.assign({}, _defaultConfig, {
      accessKeyId: creds.accessKeyId,
      secretAccessKey: creds.secretAccessKey,
      region: creds.region
    });

    // presave params
    this._awsParams = _s3Params;
    this._s3 = new AWS.S3(_s3Params);

  }

  private trim(name: string): string {
    let _name = name.replace(/\//g, '__');
    _name = _name.replace(':', '-');
    return _name;
  }

  private prepareName(name: string): string {
    let _name;
    if (name.startsWith('https://') || name.startsWith('http://')) {
      const pos = name.indexOf(`${this._params.Bucket}/`);
      if (pos >= 0) {
        _name = name.substring( pos + ( this._params.Bucket.length + 1 ) );
      } else {
        _name = name;
      }
    } else {
      _name = name;
    }

    return this.trim(_name);
  }


  save(name: string, readStream: any): Promise<any> {
    let _name = name.replace(/\//g, '__');
    _name = _name.replace(':', '-');
    const _params = Object.assign({}, this._params, { Key: _name, Body: readStream, ACL: 'public-read' });
    return this._s3.putObject(_params).promise();
  }

  read(name: string, writeStream: any): Promise<any> {
    // let _name = name.replace(/\//g, '__');
    // _name = _name.replace(':', '-');
    const _name = this.prepareName(name);

    const _params = Object.assign({}, this._params, { Key: _name });
    return this._s3.getObject(_params).promise().then( obj => {
      return new Promise( (resolve, reject) => {
        intoStream(obj.Body)
          .pipe(writeStream)
          .on('finish', resolve)
          .on('error', reject);
      });

    });
  }
  
  delete(name: string): Promise<any> {
    // const _name = name.replace(/\//g, '__');
    const _name = this.prepareName(name);
    const _params = Object.assign({}, this._params, { Key: _name });
    return this._s3.deleteObject(_params).promise();
  }

  async readAsObject(name): Promise<Object> {
    // let _name = name.replace(/\//g, '__');
    // _name = _name.replace(':', '-');
    const _name = this.prepareName(name);
    const _params = Object.assign({}, this._params, { Key: _name });
    return new Promise( (resolve, reject) => {
      return this.returnBuffer(this._s3.getObject(_params)).then(
        obj => { 
          const _json = obj.toString('utf8');
          return resolve(JSON.parse(_json));
        },
        err => reject(err)
      );
    });
  }

  async readAsBuffer(name): Promise<Buffer> {
    // let _name = name.replace(/\//g, '__');
    // _name = _name.replace(':', '-');
    const _name = this.prepareName(name);
    const _params = Object.assign({}, this._params, { Key: _name });
    return new Promise( (resolve, reject) => {
      return this.returnBuffer(this._s3.getObject(_params)).then(
        buff => {
          return resolve(<Buffer>buff)
        },
        err => reject(err)
      );
    });
  };

  private async returnBuffer(stream: any): Promise<any> {
    const _chunks = [];
    return new Promise( (resolve, reject) => {
      stream.createReadStream()
        .on(
          'data', data => {
            _chunks.push(data);
          }
        )
        .on(
          'end', () => {
            return resolve(Buffer.concat(_chunks));
          }
        );
    });
  }

  async list(prefix): Promise<IFile[]> {
    const _prefix = prefix.replace(/\//g, '__');
    const _params = Object.assign({}, this._params, { Prefix: _prefix });
    return new Promise( (resolve, reject) => {
      this._s3.listObjectsV2(_params).promise().then(
        listOfFiles => {
          const _preparedFiles = listOfFiles.Contents.map( file => {
            let _properties = {};
            for (let prop in file) {
              if (Object.prototype.hasOwnProperty.call(file, prop)) {
                if (prop !== 'Key') {
                  _properties[prop] = file[prop];
                }
              }
            }

            return {
              fullBlobName: `https://s3.${this._awsParams.region}.amazonaws.com/${_params.Bucket}/${file.Key}`,
              properties: _properties,
              metadata: { MaxKeys: listOfFiles.MaxKeys, Prefix: listOfFiles.Prefix, Name: listOfFiles.Name }
            };
          });
          return resolve(_preparedFiles);
        },
        err => reject(err)
      );
    });
  }

  private transformToStream(data: any) {
      if (data instanceof stream.Readable) {
          return data;
      } else if (Buffer.isBuffer(data)) {
          return this.convertBufferToStream(data);
      } else if (data instanceof Object) {
          return this.convertObjectIntoStream(data);
      } else if (typeof data === "string") {
          return fs.createReadStream(data);
      } else {
          throw new Error("Specified parameter has unsupported type.");
      }
  }

  private convertBufferToStream(buffer: Buffer) {
      return intoStream(buffer);
  }


  private convertObjectIntoStream(object: any): any {
      let stringifiedJson = JSON.stringify(object, null, 0);
      let buffer = new Buffer(stringifiedJson, "utf8");
      return this.convertBufferToStream(buffer);
  }

}
